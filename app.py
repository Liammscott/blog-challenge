from flask import Flask, render_template, request
import Blog
app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def results():
    title = request.form["title"]
    if  Blog.titleCheck(title) == False:
        return render_template('text_test.html', Title_in_use = 'Title already in use' )
    else:
        Blog.draftTitle(title)

        text = request.form["text"]
        Blog.draftContent(text)

        return render_template('text_test.html', Title_in_use = '' )

@app.route('/entry', methods=['GET', 'POST'])
def entry():
    entry = request.form["search"]
    content = Blog.entriesDict(entry)
    return render_template('results.html', Text=content)




if __name__ == '__main__':
     app.run()
